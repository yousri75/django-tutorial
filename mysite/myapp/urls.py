from django.urls import path

from .views import *

urlpatterns = [
    #   serve index.html
    path("", index , name = "index"),

    #   [GET, POST books] + [GET, PUT, and DELETE book]
    path("books/", BookList.as_view(), name="book-list"),
    path("books/<int:pk>/", BookDetail.as_view(), name="book-detail")

]