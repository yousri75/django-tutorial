# Django Tutorial

## Getting Started

Première chose à faire créer un environnement virtuel.

``` shell 
virtualenv -e python3.env
source .venv/bin/activate
```

Install the requirements : 

``` shell
pip install -r requirements.txt
```

Lauch the server : 

```shell
python manage.py runserver
```


